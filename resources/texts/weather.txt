<b>{place}</b>
Погода: {weather}
Температура: от {temp_min}C° до {temp_max}C°, ощущается как {feels_like}C°
Ветер: {wind_deg}°, {wind_speed} м/с{if_wind_gust}
Рассвет: {sunrise}, закат: {sunset}
Давление: {pressure} гПа
Облачность: {clouds}%