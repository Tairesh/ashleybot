from telebot import TeleBot
from telebot.types import User, Chat, Message
from telebot.apihelper import ApiException
import os
from hashlib import md5
import requests
import dice
import random
import re
from typing.re import Match
import locale
import json
from bs4 import BeautifulSoup
import urllib.request


PERMANENT_ADMINS = {31445050, }
PIDORS = set()
DOWNLOADS_DIR = os.getcwd() + '/downloads/'
GOOSE_DIR = os.getcwd() + '/goose/'
COIN = "🍋"
STICKER_CANT_DO = 'CAACAgIAAxkBAAEBbENgL8G4ZxyoJxq33Fxg7i8OkjFbXQAC3gADOtDfATCDbWmttvc3HgQ'
STICKER_FOUND_NOTHING = 'CAADAgADxgADOtDfAeLvpRcG6I1bFgQ'
STICKEr_SOMETHING_WRONG = 'CAADAgADyAADOtDfARL0PAOfBWJWFgQ'

OPENLIST_API = "https://ru.openlist.wiki//api.php?action=OlRandomPage&format=json"
OPENLIST_URL_PREFIX = "https://ru.openlist.wiki/"

FFMPEG_IN_PROCESS = False

r_ashley = re.compile(r'([^\w]|^)(эшли|эщли|ashley|єшли|ешлі|єшлі)', flags=re.IGNORECASE)
r_goose = re.compile(r'([^\w]|^)(гусь|гуся|гусю|гусем|гусе|goose|geese|гусей|гуси|гусям|гусях|гусын)(?!ниц)',
                     flags=re.IGNORECASE)
r_time_interval = re.compile(r"^(\d+)\s*(s|m|h|d|w|y|)", flags=re.IGNORECASE)

TIME_KEYS_TO_VALS = {
    's': 1,
    'm': 60,
    'h': 60*60,
    'd': 60*60*24,
    'w': 60*60*24*7,
    'y': 60*60*24*365,
}


def time_interval_to_sec(match: Match):
    val, key = match.groups()
    k = TIME_KEYS_TO_VALS[key] if key in TIME_KEYS_TO_VALS else 60
    return int(val) * k


def user_name(user: User, with_username=False):
    name = user.first_name
    if with_username and user.username:
        name += ' @' + user.username
    if user.last_name:
        name += ' ' + user.last_name
    return name


def is_permanent_admin(user: User):
    if user.id in PERMANENT_ADMINS:
        return True


def is_admin(bot: TeleBot, user: User, chat: Chat):
    if is_permanent_admin(user):
        return True

    member = bot.get_chat_member(chat.id, user.id)
    if member.can_restrict_members or member.status in {"creator", "administrator"}:
        return True

    return False


def delete_message(bot: TeleBot, message: Message):
    try:
        bot.delete_message(message.chat.id, message.message_id)
        return True
    except ApiException:
        return False


def pin_message(bot: TeleBot, chat_id: int, message_id=None) -> bool:
    try:
        if not message_id:
            bot.unpin_chat_message(chat_id)
        else:
            bot.pin_chat_message(chat_id, message_id, True)
        return True
    except ApiException:
        return False


def download_file(url: str) -> str:
    file_name = DOWNLOADS_DIR + md5(url.encode()).hexdigest()
    r = requests.get(url, timeout=2)
    with open(file_name, 'wb') as f:
        f.write(r.content)
    return file_name


def remove_file(file_name: str):
    os.remove(file_name)


def get_ext(url: str) -> str:
    try:
        return url.split('?')[0].split('.').pop()
    except Exception:
        return None


def try_download_and_send_image(bot: TeleBot, reply_to_message: Message, url: str, caption: str = None) -> bool:
    try:
        # file_name = download_file(url)
        ext = get_ext(url)
        try:
            if ext in ('gif', 'mp4'):
                bot.send_video(reply_to_message.chat.id, url, reply_to_message_id=reply_to_message.message_id)
            else:
                bot.send_photo(reply_to_message.chat.id, url, caption, reply_to_message.message_id)
            flag = True
        except Exception as e:
            print(e.__class__, e)
            flag = False
        # remove_file(file_name)
        return flag
    except Exception as e:
        print(e.__class__, e)
        return False


def send_found_nothing(bot: TeleBot, reply_to_message: Message):
    try:
        bot.send_sticker(reply_to_message.chat.id, STICKER_FOUND_NOTHING, reply_to_message.message_id)
    except ApiException:
        pass


def send_cant_do(bot: TeleBot, reply_to_message: Message):
    try:
        bot.send_sticker(reply_to_message.chat.id, STICKER_CANT_DO, reply_to_message.message_id)
    except ApiException:
        pass


def send_something_wrong(bot: TeleBot, reply_to_message: Message):
    try:
        bot.send_sticker(reply_to_message.chat.id, STICKEr_SOMETHING_WRONG, reply_to_message.message_id)
    except ApiException:
        pass


def get_command(message: Message):
    return message.text.split(' ')[0].split('@')[0].lower()


def get_atmention(message: Message):
    if '@' in message.text:
        command = message.text.split(' ')[0].split('@')
        return command[1] if len(command) > 1 else None
    return None


def get_keyword(message: Message, with_reply=True) -> str:
    keyword = message.text[len(message.text.split(' ')[0]) + 1::].replace(',', '').strip()
    if with_reply and not keyword and message.reply_to_message:
        rm = message.reply_to_message
        keyword = rm.caption if rm.caption else rm.text
    return keyword


def try_roll_and_send(bot: TeleBot, message: Message, string: str):
    try:
        d = dice.roll(string)
        bot.reply_to(message, f"{string} rolls {str(d)}")
    except dice.DiceException:
        send_cant_do(bot, message)


def balance_str(balance: int) -> str:
    locale.setlocale(locale.LC_ALL, '')
    return locale.format('%d', balance, grouping=True, monetary=True) + COIN


def pidor(user: User) -> bool:
    if user.id not in PIDORS:
        PIDORS.add(user.id)
        return True
    return False


def nepidor(user: User) -> bool:
    if user.id in PIDORS:
        PIDORS.remove(user.id)
        return True
    return False


def send_goose(bot: TeleBot, message: Message):
    bot.send_chat_action(message.chat.id, 'upload_photo')
    files = [os.path.join(GOOSE_DIR, f)
             for f in os.listdir(GOOSE_DIR)
             if os.path.isfile(os.path.join(GOOSE_DIR, f)) and not f.startswith('.')]
    file = random.choice(files)
    bot.send_photo(message.chat.id, open(file, 'rb'), reply_to_message_id=message.message_id)


def is_goose(message: Message):
    return r_goose.search(message.text)


def is_for_me(message: Message, me: User):
    chat: Chat = message.chat
    if chat.type == 'private':
        return True
    if message.reply_to_message and message.reply_to_message.from_user.id == me.id and message.reply_to_message.text:
        return True
    if r_ashley.search(message.text):
        return True

    return False


def chunks(s, n):
    """Produce `n`-character chunks from `s`."""
    for start in range(0, len(s), n):
        yield s[start:start + n]


def is_ascii(s):
    return all(ord(c) < 128 for c in s)


def is_autoforwarded_channel_message(message):
    return message.forward_from_chat and message.forward_from_chat.type == 'channel' \
           and message.from_user and message.from_user.id == 777000


def random_gulag_text():
    data = json.loads(urllib.request.urlopen(OPENLIST_API).read())['OlRandomPage']
    soup = BeautifulSoup(data['title'] + data['text']['*'], features="html.parser")

    link = soup.find('a')
    name = link.text
    url = OPENLIST_URL_PREFIX + urllib.request.url2pathname(link.attrs['href'])
    short_info = '\n'.join(map(lambda row: row[1:] if row.startswith(' ') else '\n'+row,
                               filter(lambda row: row and row != ' ',
                                      soup.find('div', {"id": "custom-person"}).text.split('\n'))))

    text = f'<a href="{url}">{name}</a>\n{short_info}'
    img = soup.find('img', {"class": "thumbimage"})
    if img:
        imgurl = OPENLIST_URL_PREFIX + img.attrs['src']
        text = f'<a href="{imgurl}">#</a> ' + text
    if len(text) > 3000:
        text = text[:3000] + '…'
    return text
