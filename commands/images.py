from telebot import TeleBot
from telebot.types import Message
import json
import urllib.request
from urllib.parse import quote

import pepe
import utils
from xml.etree import ElementTree
import random
import re
import memegen
import requests

BOOBS_API = "http://api.oboobs.ru/boobs/1/1/random"
BOOBS_MEDIA_SERVER = "http://media.oboobs.ru/"
GELBOORU_API = "https://gelbooru.com/index.php?page=dapi&s=post&q=index&tags={}"
CATS_API = "https://api.thecatapi.com/v1/images/search?"
DOGS_API = "https://dog.ceo/api/breeds/image/random"
GOOGLE_API = "https://www.googleapis.com/customsearch/v1"
GOOGLE_IMAGES_API = GOOGLE_API + "?q={}&start=1&key=%KEY%&cx=%CX%&searchType=image&gl=ru&imgSize=xxlarge"
PIXABAI_API = "https://pixabay.com/api/?key=%KEY%&orientation=horizontal&min_width=700&min_height=500&q={}"


r_anime = re.compile('аниме|anime|хентай|hentai', flags=re.IGNORECASE)
r_meme = re.compile('мем|meme', flags=re.IGNORECASE)
r_not_english = re.compile(r'([^a-zA-Z\d\s,.\-—!?%\'*])', flags=re.IGNORECASE)
r_word = re.compile(r'[\w\d\-\']+', flags=re.IGNORECASE)


def boobs(bot: TeleBot, message: Message):
    bot.send_chat_action(message.chat.id, 'upload_photo')

    data = json.loads(urllib.request.urlopen(BOOBS_API).read())[0]
    url = BOOBS_MEDIA_SERVER + data['preview']
    caption = data['model']

    utils.try_download_and_send_image(bot, message, url, caption)


def google_image(bot: TeleBot, message: Message):
    bot.send_chat_action(message.chat.id, 'upload_photo')
    keyword = utils.get_keyword(message)
    if not keyword:
        bot.reply_to(message, f"Usage example:\n`{utils.get_command(message)} child porn`", parse_mode='markdown')
        return

    data = json.loads(requests.get(GOOGLE_IMAGES_API.format(quote(keyword))).content.decode('utf-8'))
    if 'items' in data:
        for row in data['items']:
            url = row['link']
            if utils.try_download_and_send_image(bot, message, url):
                return

    utils.send_found_nothing(bot, message)


def anime(bot: TeleBot, message: Message):
    bot.send_chat_action(message.chat.id, 'upload_photo')
    if message.text.startswith('/'):
        keyword = utils.get_keyword(message)
        if not keyword:
            bot.reply_to(message, f"Usage example:\n`{utils.get_command(message)} loli`", parse_mode='markdown')
            return
    else:
        keyword = random.choice(('horse', 'loli', 'gay'))

    request_url = GELBOORU_API.format(quote(keyword))
    root = ElementTree.parse(urllib.request.urlopen(request_url)).getroot()
    posts = root.findall('post')
    random.shuffle(posts)
    for post in posts:
        url = post.attrib['file_url']
        ext = url.split('.').pop()
        if ext in {'jpg', 'jpeg', 'png', 'mp4'}:
            if utils.try_download_and_send_image(bot, message, url):
                return

    utils.send_found_nothing(bot, message)
    return


def cat(bot: TeleBot, message: Message):
    bot.send_chat_action(message.chat.id, 'upload_photo')
    api = CATS_API
    if message.text and message.text.startswith('/catgif'):
        api += 'mime_types=gif'
    data = json.loads(urllib.request.urlopen(api).read())[0]
    utils.try_download_and_send_image(bot, message, data['url'])


def dog(bot: TeleBot, message: Message):
    bot.send_chat_action(message.chat.id, 'upload_photo')
    data = json.loads(urllib.request.urlopen(DOGS_API).read())
    utils.try_download_and_send_image(bot, message, data['message'])


def meme(bot: TeleBot, message: Message, db):
    # if not utils.is_permanent_admin(message.from_user):
    #     utils.send_cant_do(message)
    #     return

    bot.send_chat_action(message.chat.id, 'upload_photo')

    file_url = None
    if message.reply_to_message:
        if message.reply_to_message.content_type == 'photo':
            file_id = message.reply_to_message.photo.pop().file_id
            file = bot.get_file(file_id=file_id)
            secrets = json.load(open('secrets.json', 'r'))
            file_url = f"https://api.telegram.org/file/bot{secrets['token']}/" + file.file_path
            if message.text.startswith('/'):
                keyword = utils.get_keyword(message)
                if keyword:
                    text = keyword
                else:
                    text = message.reply_to_message.caption
            else:
                text = message.text
        else:
            text = message.reply_to_message.text
    elif message.text.startswith('/'):
        text = utils.get_keyword(message)
    else:
        text = message.text

    tries = 0
    while tries < 10:
        tries += 1
        sentence = pepe.generate_sentence_by_text(db, text, 1)
        for ch in (',', '.', '..', '...', '*'):
            if ch in sentence:
                sentence = sentence.split(ch)[0]
        words = sentence.split(' ')
        random.shuffle(words)

        if file_url is None:
            urls = []
            for word in words:
                for subword in r_word.findall(word):
                    data = json.loads(urllib.request.urlopen(PIXABAI_API.format(quote(subword))).read())
                    if data['totalHits'] > 0:
                        for hit in data['hits']:
                            urls.append(hit['largeImageURL'])
        else:
            urls = [file_url]
        random.shuffle(urls)
        for url in urls:
            try:
                file_name = utils.download_file(url)
                try:
                    memegen.memetize(file_name, sentence)
                    bot.send_photo(message.chat.id, open(file_name, 'rb'), reply_to_message_id=message.message_id)
                    utils.remove_file(file_name)
                    return
                except Exception:
                    utils.remove_file(file_name)
            except Exception:
                pass

    utils.send_found_nothing(bot, message)
