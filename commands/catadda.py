from telebot import TeleBot
from telebot.apihelper import ApiException
from telebot.types import Message, InlineKeyboardMarkup, InlineKeyboardButton
import utils
import urllib.request
from urllib.parse import quote
from urllib.error import HTTPError
from bs4 import BeautifulSoup
import json


CATADDA_SEARCH = "https://cdda-trunk.chezzo.com/search?q={}"
CATADDA_LINK_START = "https://cdda-trunk.chezzo.com/"

NUMBERS_EMOJI = {
    1: "1️⃣",
    2: "2️⃣",
    3: "3️⃣",
    4: "4️⃣",
    5: "5️⃣",
    6: "6️⃣",
    7: "7️⃣",
    8: "8️⃣",
    9: "9️⃣",
    10: "🔟",
}


def _parse_link(bot: TeleBot, message: Message, url: str):
    try:
        page = urllib.request.urlopen(url).read()
    except urllib.error.HTTPError as e:
        text = "I can`t load item page: {}".format(e)
        bot.reply_to(message, text)
        return

    try:
        soup = BeautifulSoup(page, features="html.parser")
        div = soup.find('div', {"class": "row"}).find('div', {"class": "col-md-6"})
        title = soup.find('h4') if '/monsters/' in url else soup.find('h1')

        name = title.text
        desc = div.text.replace('\n\n\n', '\n\n').replace('\n\n\n', '\n\n').replace('>', '\n  >')
        # print(desc)
        text = f"<b>{name}</b><code>{desc}</code>"
        bot.reply_to(message, text, parse_mode='html')
    except Exception as e:
        print(e)
        utils.send_something_wrong(bot, message)


def btn_pressed(bot: TeleBot, message: Message, data: str):
    bot.send_chat_action(message.chat.id, 'typing')
    if data.startswith('cdda:'):
        data = data[5::]
        url = CATADDA_LINK_START + data
        _parse_link(bot, message.reply_to_message, url)
        utils.delete_message(bot, message)
    elif data == 'cdda_cancel':
        bot.edit_message_text(message.text.split('\n')[0] + '\n(canceled)', message.chat.id, message.message_id)
    elif data.startswith('cdda_page'):
        page, actkey = data[9::].split('_')
        print(page)
        action, keyword = actkey.split(':')
        page = int(page)
        if page < 1:
            return
        results = _get_search_results(keyword, action)
        count = len(results)
        results = results[(page-1)*10:page*10:]
        if len(results) == 0:
            return
        desc, markup = _get_page_view(results, keyword, action, maxpage=int(count/10), page=page)
        try:
            bot.edit_message_text(desc, message.chat.id, message.message_id, reply_markup=markup, parse_mode='HTML')
        except ApiException:
            pass


def _get_search_results(keyword, action):
    results = []
    page = urllib.request.urlopen(CATADDA_SEARCH.format(quote(keyword))).read()
    soup = BeautifulSoup(page, features="html.parser")
    if action == 'monster':
        ul = soup.find('ul', {"class": "list-unstyled"})
        if ul:
            lis = ul.findAll('li')
            for li in lis:
                a = li.find('a')
                results.append((a.text, a["href"]))
    else:
        divs = soup.findAll('div', {"class": "row"})
        for div in divs:
            links = div.findAll('a')
            if len(links):
                text = f'<a href="{links[0]["href"]}"><b>{links[0].text}</b></a>'
                link = links[0]["href"]
                if action == 'craft':
                    link += "/craft"
                if action == 'disassemble':
                    link += "/disassemble"
                if len(links) > 1:
                    for ll in links[1:]:
                        text += f' <a href="{ll["href"]}">[{ll.text}]</a>'

                results.append((text, link))
    return results


def _get_page_view(results, keyword, action, maxpage, page=1):
    markup = InlineKeyboardMarkup(row_width=5)
    desc = f"Search results for {action} {keyword}\n"
    btns = []
    for i, (text, link) in enumerate(results):
        btn = InlineKeyboardButton(text=NUMBERS_EMOJI[i + 1], callback_data="cdda:" + link.replace(CATADDA_LINK_START, ''))
        btns.append(btn)
        desc += NUMBERS_EMOJI[i + 1] + ' ' + text + '\n'
    desc += f"(page {page} of {maxpage+1})"
    markup.add(*btns)
    btm_row = []
    if page > 1:
        btm_row.append(InlineKeyboardButton(text="⬅️ Prev.", callback_data=f"cdda_page{page - 1}_{action}:{keyword}"))
    btm_row.append(InlineKeyboardButton(text="❌ Cancel", callback_data="cdda_cancel"))
    if page <= maxpage:
        btm_row.append(InlineKeyboardButton(text="➡ Next️️", callback_data=f"cdda_page{page + 1}_{action}:{keyword}"))
    markup.add(*btm_row)
    return desc, markup


def search(bot: TeleBot, message: Message):
    bot.send_chat_action(message.chat.id, 'typing')
    keyword = utils.get_keyword(message)
    command = utils.get_command(message).lower()
    if not keyword:
        bot.reply_to(message, f"Usage example:\n<code>{command} glazed tenderloins</code>", parse_mode='html')
        return
    action = 'view'
    if command in {'/c', '/craft'}:
        action = 'craft'
    if command in {'/disassemble', '/d', '/disasm'}:
        action = 'disassemble'
    if command in {'/m', '/mob', '/monster'}:
        action = 'monster'

    try:
        results = _get_search_results(keyword, action)

        count = len(results)
        if count == 0:
            utils.send_found_nothing(bot, message)
        elif count == 1:
            _parse_link(bot, message, results[0][1])
        else:
            desc, markup = _get_page_view(results[0:10:], keyword, action, maxpage=int(count/10))
            bot.reply_to(message, desc, reply_markup=markup, parse_mode='HTML')

    except HTTPError as e:
        bot.reply_to(message, "I can't load search page: {}".format(e))
        return


CATADDA_GIT_API = "https://api.github.com/repos/CleverRaven/Cataclysm-DDA/releases"

LINUX = 'linux'
LINUX_NAMES = {'lin', 'linux'}
WINDOWS = 'windows'
WINDOWS_NAMES = {'win', 'window', 'windows'}
OSX = 'osx'
OSX_NAMES = {'osx', 'os x', 'apple'}
ANDROID = 'android'
ANDROID_NAMES = {'android', 'phone', 'android64', 'android 64', 'android 64bit', 'android 64 bit'}
ANDROID32 = 'android32'
ANDROID32_NAMES = {'android32', 'android 32', 'android 32bit', 'android 32 bit'}
ALL_PLATFORM_NAMES = LINUX_NAMES | WINDOWS_NAMES | OSX_NAMES | ANDROID_NAMES | ANDROID32_NAMES

MODE_ALL = 'all'
MODE_VERSION = 'version'
MODE_PLATFORM = 'platform'
MODE_STABLE = 'stable'
MODE_LAST = 'last'
MODE_INVALID = 'invalid'


def cdda_release(bot: TeleBot, message: Message):
    bot.send_chat_action(message.chat.id, 'typing')
    keyword = utils.get_keyword(message).lower().strip()

    mode = MODE_ALL
    version = None
    if keyword:
        if keyword in ALL_PLATFORM_NAMES:
            mode = MODE_PLATFORM
            if keyword in LINUX_NAMES:
                version = LINUX
            elif keyword in WINDOWS_NAMES:
                version = WINDOWS
            elif keyword in OSX_NAMES:
                version = OSX
            elif keyword in ANDROID_NAMES:
                version = ANDROID
            elif keyword in ANDROID32_NAMES:
                version = ANDROID32
        elif keyword.isnumeric():
            mode = MODE_VERSION
            version = int(keyword)
            if version < 10000:
                mode = MODE_INVALID
        elif keyword == 'stable':
            mode = MODE_STABLE
        elif keyword in {'last', 'latest'}:
            mode = MODE_LAST
        elif keyword:
            mode = MODE_INVALID

    def _last_release() -> int:
        return int(json.loads(urllib.request.urlopen(CATADDA_GIT_API).read())[0]['name'].split('#').pop())

    def _links_from_assets(assets) -> dict:
        links = {
            LINUX: None,
            WINDOWS: None,
            OSX: None,
            ANDROID: None,
        }

        for asset in assets:
            if asset['label'] == 'Linux_x64 Tiles':
                links[LINUX] = asset['browser_download_url']
            elif asset['label'] == 'OSX Tiles':
                links[OSX] = asset['browser_download_url']
            elif asset['label'] == 'Windows_x64 Tiles':
                links[WINDOWS] = asset['browser_download_url']
            elif asset['label'].startswith('Android') and '64' in asset['label']:
                links[ANDROID] = asset['browser_download_url']
            elif asset['label'].startswith('Android') and '32' in asset['label']:
                links[ANDROID32] = asset['browser_download_url']
        return links

    def _send_links(name, links):
        text = "<b>Release " + name + ':</b>\n'
        for platform, link in links.items():
            text += platform + ': '
            if link:
                file = link.split('/').pop()
                text += f'<a href="{link}">{file}</a>\n'
            else:
                text += 'not compiled\n'
        bot.reply_to(message, text, parse_mode='html')

    if mode == MODE_INVALID:
        bot.reply_to(message, 'Invalid format')
        return
    elif mode == MODE_VERSION:
        delta = _last_release() - version
        page = int(delta / 100) + 1
        data = json.loads(urllib.request.urlopen(CATADDA_GIT_API + f'?page={page}&per_page=100').read())

        for release in data:
            if release['name'].endswith(keyword):
                _send_links(release['name'], _links_from_assets(release['assets']))
                return
    elif mode == MODE_STABLE:
        release = json.loads(urllib.request.urlopen(CATADDA_GIT_API + '/latest').read())
        _send_links(release['name'], _links_from_assets(release['assets']))
        return
    else:
        page = 1
        tmp_message = None
        while page < 100:
            data = json.loads(urllib.request.urlopen(CATADDA_GIT_API + f'?page={page}&per_page=100').read())
            for release in data:
                links = _links_from_assets(release['assets'])
                if (mode == MODE_PLATFORM and version in links and links[version]) \
                        or (mode == MODE_ALL and all(links.values())) \
                        or mode == MODE_LAST:
                    _send_links(release['name'], {version: links[version]} if mode == MODE_PLATFORM else links)
                    if tmp_message:
                        utils.delete_message(bot, tmp_message)
                    return
            if page == 1:
                tmp_message = bot.reply_to(message, "🤔 I did not find a suitable version on the first page,"
                                                    " please wait a little.")
            page += 1

    utils.send_found_nothing(bot, message)
