from telebot import TeleBot
from telebot.types import Message
from telebot.apihelper import ApiException
import os
import utils
import cache
from time import time
import html


def restart_cmd(bot: TeleBot, message: Message):
    if message.from_user.id in utils.PERMANENT_ADMINS:
        bot.reply_to(message, 'OK, master')
        os.system('sudo systemctl restart ashleybot.service')


def logs_cmd(bot: TeleBot, message: Message):
    if message.from_user.id in utils.PERMANENT_ADMINS:
        logs = html.escape(os.popen('tail -n 20 | sudo grep ashley /var/log/syslog').read())
        for chunk in utils.chunks(logs, 2900):
            bot.reply_to(message, f'<code>{chunk}</code>', parse_mode='html')


def pepe_info(bot: TeleBot, message: Message):
    if message.from_user.id in utils.PERMANENT_ADMINS:
        bot.reply_to(message, '<code>' + os.popen('redis-cli INFO Keyspace | grep ^db').read() + '</code>',
                     parse_mode='html')


def kick(bot: TeleBot, message: Message):
    if utils.is_admin(bot, message.from_user, message.chat) and message.reply_to_message:
        try:
            user = message.reply_to_message.from_user
            name = 'Psy-op' if message.text.startswith('/psyop') else utils.user_name(user)
            bot.kick_chat_member(message.chat.id, user.id)
            bot.reply_to(message, f"{name} kicked out")
        except ApiException:
            utils.send_cant_do(bot, message)


def ro(bot: TeleBot, message: Message):
    if utils.is_admin(bot, message.from_user, message.chat) and message.reply_to_message:
        try:
            user = message.reply_to_message.from_user
            keyword = utils.get_keyword(message, False)
            if keyword.strip() == '':
                interval = 5*60
            else:
                match = utils.r_time_interval.match(keyword)
                if match:
                    interval = utils.time_interval_to_sec(match)
                else:
                    bot.reply_to(message, "Possible values: `5m` or `10 days`", parse_mode='Markdown')
                    return
            date = round(time())+interval
            bot.restrict_chat_member(message.chat.id, user.id, until_date=date)
            bot.reply_to(message, f"{utils.user_name(user)} restricted for {interval} seconds")
        except ApiException:
            utils.send_cant_do(bot, message)


def pidor(bot: TeleBot, message: Message):
    user = message.reply_to_message.from_user if message.reply_to_message else message.from_user
    try:
        if utils.pidor(user):
            bot.reply_to(message, f"{utils.user_name(user)} becomes a pidor.")
        else:
            bot.reply_to(message, "You can't pidoring someone who is already a pidor.")

    except ApiException:
        utils.send_cant_do(bot, message)


def nepidor(bot: TeleBot, message: Message):
    user = message.reply_to_message.from_user if message.reply_to_message else message.from_user
    try:
        if utils.nepidor(user):
            bot.reply_to(message, f"{utils.user_name(user)} not a pidor anymore.")
        else:
            bot.reply_to(message, f"{utils.user_name(user)} not a pidor, you can't unpidor him.")
    except ApiException:
        utils.send_cant_do(bot, message)


def pidors(bot: TeleBot, message: Message):
    p = []
    for u in utils.PIDORS:
        if u and u in cache.known_users:
            p.append(utils.user_name(cache.known_users[u]))
    if len(p):
        bot.reply_to(message, f"Current pidors: {', '.join(p)}")
    else:
        bot.reply_to(message, "Pidors list is empty")


def chat_info(bot: TeleBot, message: Message):
    bot.reply_to(message, '`'+str(message.chat)+'`', parse_mode='markdown')


def message_info(bot: TeleBot, message: Message):
    text = str(message.reply_to_message)
    for part in utils.chunks(text, 2000):
        bot.reply_to(message, f'`{part}`', parse_mode='markdown')


def crash(bot: TeleBot, message: Message):
    if not utils.is_permanent_admin(message.from_user):
        return

    a = 0/0
    bot.reply_to(message, str(a))


def welcome_test(bot: TeleBot, message: Message):
    if not utils.is_permanent_admin(message.from_user):
        return
    import welcome
    welcome.send_welcome(bot, message, message.from_user)
