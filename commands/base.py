import json
import subprocess
from datetime import datetime, timezone, timedelta

from telebot import TeleBot
from telebot.types import Message, InlineKeyboardMarkup, InlineKeyboardButton
import utils
import re
import random
from bs4 import BeautifulSoup
import urllib.request
import urllib.error
from urllib.parse import quote
import pepe
from insultgenerator import phrases

FONOSEMANTIC_URL = "https://psi-technology.net/servisfonosemantika.php"
GOOGLE_TRANSLATE_API = "https://translation.googleapis.com/language/translate/v2"
GOOGLE_LANGS = {'hmn', 'fr', 'sv', 'sq', 'am', 'la', 'de', 'ig', 'gu', 'fa', 'ar', 'ca', 'my', 'no', 'mr', 'da', 'en',
                'eu', 'zh-CN', 'id', 'kk', 'pt', 'pa', 'mk', 'hi', 'ug', 'tl', 'ne', 'ny', 'te', 'zh-TW', 'ur', 'th',
                'xh', 'mn', 'uk', 'yo', 'gl', 'ka', 'uz', 'fy', 'hu', 'ku', 'lt', 'pl', 'ro', 'st', 'ky', 'ta', 'cy',
                'haw', 'tr', 'lo', 'be', 'fi', 'sr', 'az', 'or', 'tg', 'su', 'ps', 'zu', 'sk', 'sl', 'kn', 'si', 'km',
                'hy', 'lv', 'eo', 'et', 'af', 'ja', 'lb', 'ceb', 'he', 'bs', 'rw', 'ht', 'sn', 'bg', 'mt', 'iw', 'hr',
                'ml', 'mi', 'is', 'ha', 'ga', 'bn', 'el', 'nl', 'zh', 'mg', 'co', 'tt', 'ko', 'ms', 'ru', 'jw', 'it',
                'yi', 'tk', 'cs', 'es', 'so', 'sw', 'vi', 'gd', 'sd', 'sm'}
GOOGLE_CLOUD_KEY = ''
OPENWEATHERMAP_API = "https://api.openweathermap.org/data/2.5/weather?q={}&appid=%APPID%&lang=ru&units=metric"
WEATHER_TEMPLATE = open('resources/texts/weather.txt', 'r').read()

r_or = re.compile(r'\s+(или|or)\s+', flags=re.IGNORECASE)
r_dice = re.compile(r'(брось|кинь|roll|roll a)\s+(дайс(ы)*|кубик(и)*|кость|кости|dice|die|[\dduf]+)', flags=re.IGNORECASE)
r_dice_name = re.compile(r'дайс(ы)*|кубик(и)*|кость|кости|dice|die', flags=re.IGNORECASE)
r_fonetic = re.compile(r"(?:как звучит слово|как по твоему звучит слово|как по твоему звучит|как звучит) ("
                       r"?:\"|“|”|'|‘|’|«|»)*([абвгдеёжзийёклмнопрстуфхцчшщъыьэюя\*]+)(?:\"|“|”|'|‘|’|«|»)*",
                       flags=re.IGNORECASE)
r_not_cyrilic = re.compile(r'([^А-Яа-яёЁ*])', flags=re.IGNORECASE)
r_glasnye = re.compile(r'([ауеоыяиэё])', flags=re.IGNORECASE)


def start(bot: TeleBot, message: Message):
    bot.send_chat_action(message.chat.id, 'typing')
    bot.reply_to(message, open('resources/texts/help.txt', 'r').read(), parse_mode='html')


def donate(bot: TeleBot, message: Message):
    bot.send_chat_action(message.chat.id, 'typing')
    bot.reply_to(message, open('resources/texts/donate.txt', 'r').read(), parse_mode='markdown')


def ping(bot: TeleBot, message: Message):
    bot.send_chat_action(message.chat.id, 'typing')
    bot.reply_to(message, 'Pong!')


def del_command(bot: TeleBot, message: Message):
    if message.reply_to_message and message.reply_to_message.from_user.id == bot.get_me().id:
        utils.delete_message(bot, message.reply_to_message)


def me(bot: TeleBot, message: Message):
    keyword = utils.get_keyword(message)
    if keyword:
        bot.send_chat_action(message.chat.id, 'typing')
        bot.send_message(message.chat.id, f"<b>* {utils.user_name(message.from_user)}</b> {keyword}", parse_mode='html')
        utils.delete_message(bot, message)


def _dice_btn(dice_val):
    return InlineKeyboardButton(text=dice_val, callback_data='dice:' + dice_val)


def _dice_markup():
    markup = InlineKeyboardMarkup()
    markup.row(_dice_btn('d4'), _dice_btn('d6'), _dice_btn('d8'))
    markup.row(_dice_btn('d10'), _dice_btn('d12'), _dice_btn('d20'))
    markup.row(_dice_btn('d100'), _dice_btn('+1d'), _dice_btn('-1d'))
    return markup


def dice_roll_btn_pressed(bot: TeleBot, message: Message, data: str):
    data = data[5::]
    count = int(message.text.split(' ')[1])
    if data in {'+1d', '-1d'}:
        if data == '+1d':
            count += 1
        elif data == '-1d':
            if count == 1:
                return
            count -= 1

        bot.edit_message_text(chat_id=message.chat.id, message_id=message.message_id,
                              reply_markup=_dice_markup(), text=f"Rolling {count} dice{'s' if count > 1 else ''}:")
    else:
        keyword = str(count) + data
        bot.send_chat_action(message.chat.id, 'typing')
        utils.try_roll_and_send(bot, message.reply_to_message, keyword)
        utils.delete_message(bot, message)


def roll(bot: TeleBot, message: Message):
    bot.send_chat_action(message.chat.id, 'typing')
    if message.text.startswith('/'):
        keyword = utils.get_keyword(message)
    else:
        keyword = r_dice.search(message.text).group(2)
        if r_dice_name.match(keyword):
            keyword = None
    if not keyword:
        bot.reply_to(message, 'Rolling 1 dice:', reply_markup=_dice_markup())
        return
    utils.try_roll_and_send(bot, message, keyword)


def say(bot: TeleBot, message: Message):
    bot.send_chat_action(message.chat.id, 'typing')
    keyword = utils.get_keyword(message)
    bot.reply_to(message, keyword)


def choice(bot: TeleBot, message: Message):
    bot.send_chat_action(message.chat.id, 'typing')
    keyword = utils.get_keyword(message)
    if keyword[-1::] == '?':
        keyword = keyword[:-1:]
    if not keyword:
        bot.reply_to(message, "Usage example\n`/choice to be or not to be?`", parse_mode='markdown')
        return
    variants = r_or.split(keyword)
    for var in variants:
        if r_or.search(f" {var} "):
            variants.remove(var)
    sel = random.choice(variants)
    bot.reply_to(message, sel)


def _authorize():
    google_cloud_key_command = "export GOOGLE_APPLICATION_CREDENTIALS=google-cloud-secret.json; " \
                               "gcloud auth application-default print-access-token"
    global GOOGLE_CLOUD_KEY
    GOOGLE_CLOUD_KEY = subprocess.Popen(google_cloud_key_command, shell=True,
                                        stdout=subprocess.PIPE).stdout.read().decode().strip()


def _translate(q, target_language, recursive=True) -> tuple:
    if GOOGLE_CLOUD_KEY == '':
        _authorize()
    data = json.dumps({'q': q, 'target': target_language, 'format': 'text'}).encode('utf-8')
    req = urllib.request.Request(GOOGLE_TRANSLATE_API, data, {
        'Authorization': 'Bearer ' + GOOGLE_CLOUD_KEY,
        'Content-Type': 'application/json; charset=utf-8',
    })

    try:
        with urllib.request.urlopen(req) as response:
            result = json.loads(response.read().decode())['data']['translations'][0]
            return result['detectedSourceLanguage'], result['translatedText']
    except urllib.error.HTTPError as e:
        if recursive:
            _authorize()
            return _translate(q, target_language, False)
        else:
            raise e


def trans(bot: TeleBot, message: Message):
    bot.send_chat_action(message.chat.id, 'typing')
    keyword = utils.get_keyword(message, False)
    words = keyword.split(' ')
    target_lang = 'en'
    if len(words) > 1 and words[0].lower() in GOOGLE_LANGS:
        target_lang = words[0].lower()
        text = ' '.join(words[1:])
    else:
        rm = message.reply_to_message
        if words[0].lower() in GOOGLE_LANGS and rm:
            target_lang = words[0].lower()
            text = rm.caption if rm.caption else rm.text
        else:
            text = keyword if not rm else (rm.caption if rm.caption else rm.text)

    if not text:
        example = f'Usage example: <code>{utils.get_command(message)} Text to translate</code>\n' \
                  'By default it translated to Russian. You can set target language as first argument, for example ' \
                  f'translate to Italian: <code>{utils.get_command(message)} IT Text to translate</code>'
        bot.reply_to(message, example, parse_mode='html')
        return

    detected_lang, translated_text = _translate(text, target_lang)
    if detected_lang == target_lang:
        if detected_lang == 'en':
            target_lang = 'ru'
        else:
            target_lang = 'en'
        _, translated_text = _translate(text, target_lang)
    bot.reply_to(message, f"<code>{translated_text}</code>", parse_mode='html')


def _uy2oe(string: str) -> str:
    string = string.lower()
    if string[-3::] in {'ший', 'чий', 'щий'}:
        return string[:-2:] + 'ее'
    else:
        return string[:-2:] + 'ое'


def _get_views(keyword: str) -> list:
    data = f"slovo={quote(keyword)}&sub=".encode('ascii')
    req = urllib.request.Request(FONOSEMANTIC_URL, data, {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64)'})
    with urllib.request.urlopen(req) as response:
        page = response.read()
        soup = BeautifulSoup(page, features="html.parser")
        table = soup.find('table', {'class': 'prais'})
        trs = table.find_all_next('tr')
        views = []
        for tr in trs:
            tds = tr.find_all('td')
            if len(tds):
                count = float(tds[1].text.replace(',', '.'))
                dist = abs(count-3)
                view = tds[3].text
                if view != "Не выражен":
                    views.append((dist, view))
        views = list(sorted(views, key=lambda d: d[0], reverse=True))
        return views


def fonosemantic(bot: TeleBot, message: Message):
    bot.send_chat_action(message.chat.id, 'typing')
    if message.text.startswith('/'):
        keyword = utils.get_keyword(message).lower()
    else:
        try:
            keyword = r_fonetic.search(message.text).group(1).lower()
        except Exception:
            keyword = None
    if not keyword:
        bot.reply_to(message, 'Укажи слово для оценки')
        return
    if r_not_cyrilic.search(keyword):
        bot.reply_to(message, 'Я пока могу только в кириллицу :(')
        return
    if '*' not in keyword:
        gl = r_glasnye.search(keyword)
        if not gl:
            bot.reply_to(message, 'В слове должны быть гласные :-P')
            return
        pos = gl.span()[1]
        keyword = keyword[:pos:] + '*' + keyword[pos::]
    keyword_label = keyword.replace('*', '')
    views = _get_views(keyword)

    if message.text.startswith('/fs_full'):
        values = [f"{_uy2oe(b)} ({a:.2f})" for a, b in views]
        bot.reply_to(message, '\n'.join(values))
    else:
        if len(views) > 1:
            val1, val2 = _uy2oe(views[0][1]), _uy2oe(views[1][1])
            reply = f"как нечто {val1} и {val2}"
        elif len(views) == 1:
            val = _uy2oe(views[0][1])
            reply = f"как нечто {val}"
        else:
            reply = "абсолютно нейтрально"
        bot.reply_to(message, f"Слово «{keyword_label}» звучит {reply}")


def random_reply(bot: TeleBot, message: Message, db):
    bot.send_chat_action(message.chat.id, 'typing')
    if message.text.startswith('/'):
        if message.reply_to_message:
            text = message.reply_to_message.text
        else:
            text = utils.get_keyword(message)
    else:
        text = message.text

    sentence = None
    if text:
        sl = len(text) // 200
        if sl < 2:
            sl = 2
        elif sl > 20:
            sl = 20
        sentence = pepe.generate_sentence_by_text(db, text, sentences_limit=sl)
    if not sentence:
        sentence = pepe.generate_sentence(db)[0]

    bot.reply_to(message, pepe.capitalise(sentence))


def forget_text(bot: TeleBot, message: Message, db):
    if not utils.is_permanent_admin(message.from_user):
        return
    if not message.reply_to_message or not message.reply_to_message.text \
            or message.reply_to_message.from_user.id != bot.get_me().id:
        return

    bot.send_chat_action(message.chat.id, 'typing')

    trigrams = pepe.remove_all_trigrams(db, message.reply_to_message.text)
    text = "These keys have been successfully forgotten: " + ", ".join(['->'.join(t) for t in trigrams])
    for t in utils.chunks(text, 3000):
        bot.reply_to(message, t)


def gulag_random(bot: TeleBot, message: Message):
    bot.send_chat_action(message.chat.id, 'typing')
    bot.reply_to(message, utils.random_gulag_text(), parse_mode='HTML')


def cho(bot: TeleBot, message: Message):
    bot.reply_to(message, 'чё' + random.choice(('', '', '', '', '', '', '?', '!', '?', '!', '!!!', '...')))


def shrug(bot: TeleBot, message: Message):
    bot.reply_to(message, '¯\\_(ツ)_/¯')


def weather(bot: TeleBot, message: Message):
    bot.send_chat_action(message.chat.id, 'typing')
    if message.text.startswith('/'):
        if message.reply_to_message:
            text = message.reply_to_message.text
        else:
            text = utils.get_keyword(message)
    else:
        text = message.text
    if not text:
        bot.reply_to(message, "Usage example: <code>/weather New York</code>", parse_mode="HTML")
        return

    try:
        data = json.loads(urllib.request.urlopen(OPENWEATHERMAP_API.format(quote(text))).read())
        tz = timezone(timedelta(seconds=data['timezone']))
        result = WEATHER_TEMPLATE.format(
            place=data['name'],
            weather=', '.join([w['description'] for w in data['weather']]),
            temp_min=round(data['main']['temp_min']),
            temp_max=round(data['main']['temp_max']),
            feels_like=round(data['main']['feels_like']),
            pressure=data['main']['pressure'],
            wind_deg=data['wind']['deg'],
            wind_speed=data['wind']['speed'],
            if_wind_gust=f", порывы до {data['wind']['gust']} м/с"
                         if 'gust' in data['wind'] else '',
            clouds=data['clouds']['all'],
            sunrise=datetime.fromtimestamp(data['sys']['sunrise'], tz).strftime("%H:%M"),
            sunset=datetime.fromtimestamp(data['sys']['sunset'], tz).strftime("%H:%M"),
        )
        bot.reply_to(message, result, parse_mode="HTML")
    except urllib.error.HTTPError as e:
        if e.code == 404:
            bot.reply_to(message, "Place not found :-(")
            return
        raise e


def random_insult(bot: TeleBot, message: Message):
    bot.send_chat_action(message.chat.id, 'typing')
    if message.reply_to_message and message.reply_to_message.from_user:
        user = message.reply_to_message.from_user
    else:
        user = message.from_user
    name = utils.user_name(user)
    pronoun = 'he'
    if user.id == bot.get_me().id:
        pronoun = 'she'
    r = random.randint(0, 3)
    if r == 0:
        text = phrases.get_simple_insult(name)
    elif r == 1:
        text = phrases.get_so_insult(name)
    elif r == 2:
        text = phrases.get_so_insult_with_action(name, pronoun)
    elif r == 3:
        text = phrases.get_so_insult_with_action_and_target(name, pronoun)
    bot.reply_to(message, text)
