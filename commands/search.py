import json

import requests
from telebot import TeleBot
from telebot.types import Message
import utils
import googlesearch
from urllib.parse import quote


YOUTUBE_API = "https://www.googleapis.com/youtube/v3/search?q={}&key=%KEY%&cx=%CX%"


def google_search(bot: TeleBot, message: Message):
    bot.send_chat_action(message.chat.id, 'typing')
    keyword = utils.get_keyword(message)
    if not keyword:
        bot.reply_to(message, f"Usage example:\n<code>{utils.get_command(message)} how to kidnap a loli</code>",
                     parse_mode='html')
        return
    try:
        url = next(googlesearch.search(keyword, stop=1))
        bot.reply_to(message, url)
    except StopIteration:
        utils.send_found_nothing(bot, message)


def youtube_search(bot: TeleBot, message: Message):
    bot.send_chat_action(message.chat.id, 'typing')
    keyword = utils.get_keyword(message)
    if not keyword:
        bot.reply_to(message, f"Usage example:\n<code>{utils.get_command(message)} rickroll</code>", parse_mode='html')
        return

    data = json.loads(requests.get(YOUTUBE_API.format(quote(keyword))).content.decode('utf-8'))
    if 'items' in data:
        for row in data['items']:
            if row['id']['kind'] == 'youtube#video':
                url = "https://www.youtube.com/watch?v=" + row['id']['videoId']
                bot.reply_to(message, url)
                return

    utils.send_found_nothing(bot, message)
