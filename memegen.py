from wand.image import Image
from wand.color import Color
from wand.drawing import Drawing


def split_to_lines(sentence: str) -> list:
    words = sentence.split(' ')
    lines = []
    current_line = []
    for word in words:
        if len(current_line) == 0 or len(' '.join(current_line)) + len(word) < 25:
            current_line.append(word)
        else:
            lines.append(' '.join(current_line))
            current_line = [word]
    if len(current_line):
        lines.append(' '.join(current_line))
    return lines


def _middle_color(img: Image) -> tuple:
    blob = img.make_blob(format='RGB')
    reds = []
    greens = []
    blues = []
    for cursor in range(img.width * img.height * 2, img.width * img.height, 3):
        reds.append(blob[cursor])
        greens.append(blob[cursor + 1])
        blues.append(blob[cursor + 2])
    red_middle = sum(reds) / (img.width * img.height)
    green_middle = sum(greens) / (img.width * img.height)
    blue_middle = sum(blues) / (img.width * img.height)
    return red_middle, green_middle, blue_middle


def memetize(file_name, sentence):
    with Image(filename=file_name) as img:
        black = False
        red_middle, green_middle, blue_middle = _middle_color(img)
        if red_middle > 200 or blue_middle > 200 or green_middle > 200:
            black = True

        with Drawing() as context:
            lines = split_to_lines(sentence)
            context.text_alignment = 'center'
            context.font = 'fonts/3952.ttf'
            max_len = len(max(lines, key=lambda el: len(el)))
            size = 1.5 * img.width / max_len

            context.font_size = size
            metrics = context.get_font_metrics(img, sentence)

            x = int(img.width / 2)
            y = int(img.height - metrics.character_height/2)

            for line in reversed(lines):
                context.fill_color = Color('#000' if black else '#fff')
                context.stroke_color = Color('#fff' if black else '#000')
                context.stroke_width = 2
                context.text(x, y, line)
                y -= int(metrics.character_height)
                if y < 0:
                    break
            context(img)

            img.format = 'jpeg'
            img.save(filename=file_name)


if __name__ == '__main__':
    memetize('goose/vertical.jpg', 'Гусь пидор')
