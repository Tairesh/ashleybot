from telebot import TeleBot
# from telebot.apihelper import ApiException
from telebot.types import CallbackQuery, Message, User, InlineKeyboardMarkup, InlineKeyboardButton
import utils


PHOTO_WELCOME = "https://tairesh.xyz/img/welcome.jpg"


def send_welcome(bot: TeleBot, message: Message, newbie: User):
    if newbie.is_bot:
        return
    # try:
    #     bot.restrict_chat_member(message.chat.id, newbie.id)
    # except ApiException:
    #     pass

    markup = InlineKeyboardMarkup()
    markup.add(InlineKeyboardButton(text="🍞 flat bread (filthy)", callback_data="captcha:bread"),
               InlineKeyboardButton(text="🧼 soap (10)", callback_data="captcha:soap"))
    markup.add(InlineKeyboardButton(text="🚫 /ban", callback_data="captcha:ban"))

    # text = f"Привет {utils.user_name(newbie, True)}, " + \
    #        "добро пожаловать в группу Cataclysm DDA.\n\n" + \
    #        "`Хлеб (грязное)` с параши или `мыло (10)` со стола?"
    # bot.reply_to(message, text, parse_mode='markdown')
    bot.send_photo(message.chat.id, PHOTO_WELCOME, reply_markup=markup, reply_to_message_id=message.message_id)


def confirm(bot: TeleBot, call: CallbackQuery):
    # try:
    #     bot.restrict_chat_member(call.message.chat.id, call.from_user.id, None,
    #                              True, True, True, True, True, True, True, True)
    # except ApiException:
    #     pass

    if call.data.endswith('soap'):
        value = '🧼 мыло'
    elif call.data.endswith('bread'):
        value = '🍞 хлеб'
    else:
        value = "🚫 бан"
    text = f"Привет {utils.user_name(call.from_user)}, " + \
           "добро пожаловать в группу Cataclysm DDA.\n" + \
           f"Предпочтения в пище: `{value}`"
    utils.delete_message(bot, call.message)
    bot.send_message(call.message.chat.id, text, parse_mode='markdown')
