#!venv/bin/python3
import traceback
from telebot import apihelper, TeleBot
import json
import utils
import cache
import redis
import pepe
import welcome
from commands import COMMANDS
import commands.base
import commands.admin
import commands.search
import commands.images
import commands.catadda
from inspect import signature

catachat_old = -1001395369125
catachat = -1001150487023
catachat_eng = -1001370773011
test_chat = -1001298015134
df_chat = -1001323036018

welcome_chats = {catachat, catachat_old}

PIDORS_BLACKLIST = {'/a', '/anime', '/boobs'}

apihelper.RETRY_ON_ERROR = True


def set_handlers(bot: TeleBot):
    db = redis.StrictRedis(host='localhost', port=6379, db=0)
    me = bot.get_me()
    cache.known_users[me.id] = me

    @bot.callback_query_handler(func=lambda call: True)
    def callback_inline(call):
        if call.from_user.id not in cache.known_users:
            cache.known_users[call.from_user.id] = call.from_user
        if call.message.chat.id not in cache.known_chats:
            cache.known_chats[call.message.chat.id] = call.message.chat

        if call.message and call.message.reply_to_message:
            if not call.data.startswith("captcha") and call.message.reply_to_message.from_user.id != call.from_user.id:
                utils.pidor(call.from_user)
                return

            if call.data.startswith("dice:"):
                commands.base.dice_roll_btn_pressed(bot, call.message, call.data)
            elif call.data.startswith("cdda"):
                commands.catadda.btn_pressed(bot, call.message, call.data)
            elif call.data.startswith("captcha") and call.message.reply_to_message.new_chat_members:
                newbies = map(lambda u: u.id, call.message.reply_to_message.new_chat_members)
                if call.from_user.id in newbies:
                    welcome.confirm(bot, call)
                else:
                    if call.data.endswith('ban') and utils.is_admin(bot, call.from_user, call.message.chat):
                        for newbie in call.message.reply_to_message.new_chat_members:
                            bot.kick_chat_member(call.message.chat.id, newbie.id)
                            utils.delete_message(bot, call.message)
                            bot.send_message(call.message.chat.id, f"{utils.user_name(newbie)} kicked out")
                    else:
                        if utils.pidor(call.from_user):
                            bot.send_message(call.message.chat.id,
                                             f"{utils.user_name(call.from_user)} becomes a pidor.")

    @bot.message_handler(func=lambda m: m.text and not m.forward_from_chat, content_types=['text'])
    def handle(message):
        if message.from_user.id not in cache.known_users:
            cache.known_users[message.from_user.id] = message.from_user
        if message.chat.id not in cache.known_chats:
            cache.known_chats[message.chat.id] = message.chat

        try:
            if message.text.startswith('/'):
                command = utils.get_command(message)
                if message.from_user.id in utils.PIDORS and command in PIDORS_BLACKLIST:
                    return
                mention = utils.get_atmention(message)
                if mention is None or mention == me.username:
                    if command in COMMANDS:
                        func = COMMANDS[command]
                        if len(signature(func).parameters) == 3:
                            func(bot, message, db)
                        else:
                            func(bot, message)
            else:
                if utils.is_for_me(message, me):
                    if commands.base.r_dice.search(message.text):
                        commands.base.roll(bot, message)
                    elif commands.base.r_fonetic.search(message.text):
                        commands.base.fonosemantic(bot, message)
                    elif commands.images.r_anime.search(message.text):
                        commands.images.anime(bot, message)
                    elif commands.images.r_meme.search(message.text):
                        commands.images.meme(bot, message, db)
                    else:
                        commands.base.random_reply(bot, message, db)
                pepe.train(db, message.text)
        except Exception:
            if message.text.startswith('/'):
                utils.send_something_wrong(bot, message)

            report = str(message) + '\n\n' + traceback.format_exc()
            for admin in utils.PERMANENT_ADMINS:
                for chunk in utils.chunks(report, 2900):
                    bot.send_message(admin, chunk)
            # traceback.print_exc()

    @bot.message_handler(content_types=['new_chat_members'])
    def new_chat_members(message):
        if message.chat.id not in cache.known_chats:
            cache.known_chats[message.chat.id] = message.chat

        for newbie in message.new_chat_members:
            if newbie.id == me.id:
                commands.base.start(bot, message)
            elif message.chat.id in welcome_chats:
                welcome.send_welcome(bot, message, newbie)


def run():
    secrets = json.load(open('secrets.json', 'r'))
    commands.images.GOOGLE_IMAGES_API = commands.images.GOOGLE_IMAGES_API.replace('%KEY%', secrets['google_apikey'])\
                                                                         .replace('%CX%', secrets['google_cx'])
    commands.images.PIXABAI_API = commands.images.PIXABAI_API.replace('%KEY%', secrets['pixabay_apikey'])
    commands.search.YOUTUBE_API = commands.search.YOUTUBE_API.replace('%KEY%', secrets['google_apikey'])\
                                                             .replace('%CX%', secrets['google_cx'])
    commands.base.OPENWEATHERMAP_API = commands.base.OPENWEATHERMAP_API.replace('%APPID%',
                                                                                secrets['openweathermap_appid'])

    if 'proxy' in secrets:
        apihelper.proxy = {'https': secrets['proxy']}
    bot = TeleBot(secrets['token'], skip_pending=secrets['skip_pending'])
    set_handlers(bot)

    for admin in utils.PERMANENT_ADMINS:
        bot.send_message(admin, 'I restarted, look at logs if it was unexpected!')

    bot.infinity_polling()


if __name__ == '__main__':
    # noinspection PyBroadException
    try:
        run()
    except Exception:
        traceback.print_exc(file=open("last_crash.txt", 'w'))
        traceback.print_exc()
